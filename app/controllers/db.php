<?php 
	/**
	* DbConfig
	* @function => class responsible for db connections
	*/
	class DbConfig{
		private $mainConn;
		
		function __construct(){};

		/**
		 * Main connect function
		 * @return  db connection handler
		 */
		function init(){
			require_once '../dbconfig.php';

			//connect to the db
			$this->mainConn = new mysqli(HOST,USERNAME,PASSWORD,DB_NAME);

			//check for errors
			if(mysqli_connect_errno()){
				echo "Database connection failed. Reason: " . mysqli_connect_error();
			}

			return $this->mainConn;

		}
	}
?>