<?php
	class dbMethods{
		private $conn;
		
		function __construct(){
			require_once 'db.php';

			$db = new dbConfig();
			$this->conn = $db->init();
		}

		//function for fetching single record frm db
		//
		public function getSingleRow($query){
			$res = $this->conn->query($query.' LIMIT 1') or die($this->conn->error.__LINE__);
        	return $result = $res->fetch_assoc();
		}

		//get all rows from a table

		public function getAllRows($query){
			$res = $this->conn->prepare($query);
			$res->execute();
			$result = $res->get_result();
	        $res->close();
	        return $result;
		}

		//count rows in the table
		public function countRows($tableName) {
	        $res = $this->conn->query('SELECT COUNT(*) as total FROM '.$tableName) or die($this->conn->error.__LINE__);
	        return $result = $res->fetch_assoc();
    	}

    	//insert a record into the table;
    	public function addRecord($obj, $column_names, $table) {

	        $c = (array) $obj;
	        $keys = array_keys($c);
	        $columns = '';
	        $values = '';
	        foreach($column_names as $desired_key){ // Check the obj received. If blank insert blank into the array.
	           if(!in_array($desired_key, $keys)) {
	                $$desired_key = '';
	            }else{
	                $$desired_key = $c[$desired_key];
	            }
	            $columns = $columns.$desired_key.',';
	            $values = $values."'".$$desired_key."',";
	        }
	        $query = "INSERT INTO ".$table."(".trim($columns,',').") VALUES(".trim($values,',').")";

	        $r = $this->conn->query($query) or die($this->conn->error.__LINE__);

	        if ($r) {
	            $new_row_id = $this->conn->insert_id;
	            return $new_row_id;
	            } else {
	            return NULL;
	        }
    	}

    	/**
    	 * remove record from table
    	 */
    	public function deleteRecord($table, $where) {
            $what = "";
            foreach ($where as $key => $value) {
                $what .= " and " .$key. " = '".$value."' ";
            }

            $query = "DELETE FROM $table WHERE 1=1 ".$what;

            $r = $this->conn->query($query) or die($this->conn->error.__LINE__);

            if($r){
                $response = "successfully deleted";
            }else{
                $response = NULL;
            }

        	return $response;
    	}

    	/**
    	 * edit/update a record
    	 */
    	public function updateRecord($columnsArray, $table, $where) {

            $a = array();
            $w = "";
            $c = "";
            foreach ($where as $key => $value) {
                $w .= " and " .$key. " = '".$value."' ";
            }
            foreach ($columnsArray as $key => $value) {
                $c .= $key. " = '".$value."', ";
            }
                $c = rtrim($c,", ");


            $query = "UPDATE $table SET $c WHERE 1=1 ".$w;

            $r = $this->conn->query($query) or die($this->conn->error.__LINE__);

            if($r){
                $response = "success";
            }else{
                $response = NULL;
            }

        	return $response;
    	}



    	/**
    	 * functions that manage session storage
    	 */
    	public function readSession(){
    		if(!isset($_SESSION)){ //check if there's no active session
    			session_start();
    		}

    		$returnValue = array();

    		if(isset($_SESSION['_id'])){
    			//add session values to the return var 
    			$returnValue["_id"] = $_SESSION['_id'];
		        $returnValue["createdAt"] = $_SESSION['createdAt'];
		        $returnValue["modifiedAt"] = $_SESSION['modifiedAt'];
    		} else{
    			//if nothing is in the session return empty values for all properties
    			$returnValue["_id"] = '';
		        $returnValue["createdAt"] = '';
		        $returnValue["modifiedAt"] = '';
    		}

    		return $returnValue;
    	}

    	//function for destroying a session
    	//
    	public function destroySession(){
		    if (!isset($_SESSION)) {
		    session_start();
		    }
		    if(isSet($_SESSION['_id'])){
		        unset($_SESSION['_id']);
		        unset($_SESSION['createdAt']);
		        unset($_SESSION['modifiedAt']);

		        $meta= 'info';

		        if(isSet($_COOKIE[$meta])){
		            setcookie ($meta, '', time() - $cookie_time);
		        }
		        $returnValue="Session Deleted successfully...";
		    } else {
		        $returnValue = "No existing session...";
		    }

		    return $returnValue;
		}
	}
?>