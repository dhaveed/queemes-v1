<?php
//Gets all the admins in the database
/*$app->get('/countMember', function() use ($app){
    $db = new DbHandler();
    $response = array();
    $table_name = "users";
    $resp = $db->countRecords($table_name);

    $response["status"] = "success";
    $response["count"] = $resp['total'];

    echoResponse(200, $response);

});

$app->get('/viewMembers', function() {
    $db = new DbHandler();
    $response = array();
    $resp = $db->getAllRecords("SELECT * FROM users WHERE 1");

    $response["status"] = "success";
    $response["users"] = array();

    while ($users = $resp->fetch_assoc()) {
                $tmp = array();
                $tmp["_id"] = $users["_id"];
                $tmp["fullName"] = $users["fullName"];
                $tmp["matNumber"] = $users["matNumber"];
                $tmp["lmuMail"] = $users["lmuMail"];
                $tmp["emailAddress"] = $users["emailAddress"];
                $tmp["nickName"] = $users["nickName"];
                $tmp["gender"] = $users["gender"];
                $tmp["bday"] = $users["bday"];
                $tmp["bmonth"] = $users["bmonth"];
                $tmp["bYear"] = $users["bYear"];
                $tmp["facebookName"] = $users["facebookName"];
                $tmp["twitterName"] = $users["twitterName"];
                $tmp["residenceAddress"] = $users["residenceAddress"];
                $tmp["regNum"] = $users["regNum"];
                $tmp["state"] = $users["state"];
                $tmp["homeTown"] =$users["homeTown"];
                $tmp["hallName"] =$users["hallName"];
                $tmp["roomNum"] =$users["roomNum"];
                $tmp["sLevel"] = $users["sLevel"];
                $tmp["interest"] = $users["interest"];
                $tmp["suggestion"] = $users["suggestion"];
                $tmp["website"] = $users["website"];
                $tmp["servUnit"] = $users["servUnit"];
                $tmp["BBpin"] = $users["BBpin"];
                $tmp["phone"] = $users["phone"];
                $tmp["limeUser"] = $users["limeUser"];
                $tmp["interestedField"] = $users["interestedField"];
                $tmp["instaName"] = $users["instaName"];
                $tmp["ipAddress"] = $users["ipAddress"];
                $tmp["dateCreated"] = $users["dateCreated"];
                $tmp["dateModified"] = $users["dateModified"];
                array_push($response["users"], $tmp);
            }
    echoResponse(200, $response);
});*/

$app->post('/signUp', function($req,$res,$args){
    $response = array();
    $r = json_decode($args['details']);
    // $r = json_decode($app->request->getBody());
    verify(array('firstName','lastName', 'email', 'phone', 'matric_number', 'password'),$r);
    $db = new dbMethods();
    $firstName = $r->firstName;
    $lastName = $r->lastName;
    $email = $r->email;
    $phone = $r->phone;
    $matric_number = $r->matric_number;
    $password = $r->password;
    /*$birthday = $r->birthday;
    $facebookName = $r->facebookName;
    $twitterName = $r->twitterName;
    $residenceAddress = $r->residenceAddress;
    $regNum = $r->regNum;
    $state = $r->state;
    $originState = $r->originState;
    $homeTown = $r->homeTown;
    $hallName = $r->hallName;
    $roomNum = $r->roomNum;
    $sLevel = $r->sLevel;
    $interest = $r->interest;
    $suggestion = $r->suggestion;
    $website = $r->website;
    $servUnit = $r->servUnit;
    $BBpin = $r->BBpin;
    $phone = $r->phone;
    $limeUser = $r->limeUser;
    $instaName = $r->instaName;
    $interestedField = $r->interestedField;
    $ipAddress = $r->ipAddress;

    $r->bday = substr($birthday, 6, 2);
    $r->bmonth = substr($birthday, 4, 2);
    $r->bYear = substr($birthday, 0, 4);

    $bday = $r->bday;
    $bmonth = $r->bmonth;
    $bYear = $r->bYear;*/

    // yyyymmdd
    // 012345678
    // 
    $fullName = $firstName . ' ' . $lastName;


    $isUserExists = $db->getSingleRow("select 1 from customers where matric_number='$matric_number' or fullName='$fullName'	");
    if(!$isUserExists){
            $table_name = "customers";
            $column_names = array('_id', 'fullName', 'email', 'password', 'matric_staffid'/*, 'gender', 'bday', 'bmonth', 'bYear', 'birthday', 'facebookName', 'twitterName', 'residenceAddress', 'regNum', 'state', 'originState', 'homeTown', 'hallName', 'roomNum', 'sLevel', 'interest', 'suggestion', 'website', 'servUnit', 'BBpin', 'phone', 'limeUser', 'instaName', 'interestedField', 'ipAddress'*/);
            $result = $db->addRecord($r, $column_names, $table_name);
            if ($result != NULL) {
            $response["status"] = "success";
            $response["message"] = "Your profile has been saved";
            echoResponse(200, $response);
        } else {
            $response["status"] = "error";
            $response["message"] = "Failed to submit profile. Please try again";
            echoResponse(201, $response);
        }
    }else{
        $response["status"] = "error";
        $response["message"] = "Profile has already been saved!";
       // $response["test"] = $bYear;
        echoResponse(201, $response);

    }
});

/*$app->post('/viewMember', function() use ($app){
    $response = array();
    $r = json_decode($app->request->getBody());
    verifyRequiredParams(array('memberId'),$r);
    $db = new DbHandler();

    $id = $r->memberId;

    $resp = $db->getOneRecord("select * from users where _id=$id");

    if($resp != NULL){

        $response['status'] = "success";
        $response["member"] = $resp;

        echoResponse(200, $response);
    }else{
        $response['status'] = "error";
        $response['message'] ="Member not failed";
        echoResponse(201, $response);
    }

});

//METHODS IN REST: GET, POST, PUT
$app->put('/editMember/:id', function($id) use ($app) {
    $response = array();
    $r = json_decode($app->request->getBody());
    $condition = array('_id'=>$id);
     verifyRequiredParams(array('fullName', 'matNumber', 'lmuMail', 'gender',  'birthday', 'residenceAddress', 'regNum', 'state', 'sLevel', 'phone', 'interestedField'),$r);
    $db = new DbHandler();
    $fullName = $r->fullName;
    $matNumber = $r->matNumber;
    $lmuMail = $r->lmuMail;
    $emailAddress = $r->emailAddress;
    $nickName = $r->nickName;
    $gender = $r->gender;
    $bday = $r->bday;
    $bmonth = $r->bmonth;
    $bYear = $r->bYear;
    $birthday = $r->birthday;
    $facebookName = $r->facebookName;
    $twitterName = $r->twitterName;
    $residenceAddress = $r->residenceAddress;
    $regNum = $r->regNum;
    $state = $r->state;
    $homeTown = $r->homeTown;
    $hallName = $r->hallName;
    $roomNum = $r->roomNum;
    $sLevel = $r->sLevel;
    $interest = $r->interest;
    $suggestion = $r->suggestion;
    $website = $r->website;
    $servUnit = $r->servUnit;
    $BBpin = $r->BBpin;
    $phone = $r->phone;
    $limeUser = $r->limeUser;
    $instaName = $r->instaName;
    $interestedField = $r->interestedField;        
            $table_name = "users";
            $column_names = array('fullName', 'matNumber', 'lmuMail', 'emailAddress', 'nickName', 'gender', 'bday', 'bmonth', 'bYear', 'birthday', 'facebookName', 'twitterName', 'residenceAddress', 'regNum', 'state', 'homeTown', 'hallName', 'roomNum', 'sLevel', 'suggestion', 'website', 'servUnit', 'BBpin', 'phone', 'limeUser', 'instaName', 'interestedField');
            $result = $db->updateTable($r,$table_name,$condition);
            if ($result != NULL) {
            $response["status"] = "success";
            $response["message"] = "Profile update was successfull";
            echoResponse(200, $response);
        } else {
            $response["status"] = "error";
            $response["message"] = "Failed to edit profile. Please try again";
            echoResponse(201, $response);
        }
});

$app->delete('/deleteMember/:id', function($id) use ($app) {
    $response = array();
    $r = json_decode($app->request->getBody());
    $condition = array('_id'=>$id);
    $db = new DbHandler();
            $table_name = "users";
            $result = $db->deleteTable($table_name,$condition);
            if ($result != NULL) {
            $response["status"] = "success";
            $response["message"] = "Delete profile was successfull";
            echoResponse(200, $response);
        } else {
            $response["status"] = "error";
            $response["message"] = "Failed to delete Profile. Please try again";
            echoResponse(201, $response);
        }
});*/
?>
 