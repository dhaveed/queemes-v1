<?php
	
	namespace App\Controllers;

	use Slim\Views\Twig as View;

	/**
	* RoutesController Class
	* function: Handles all the routing middleware for queemes-v1
	* author: dhaveed adegoke.david@lmu.edu.ng
	*/
	class RoutesController extends BaseController{

		public function __construct(View $view){
			$this->view = $view;
		}

		public function basic($req,$resp,$args){     
			//$page = $req->getAttribute('page') . '.twig';
			$page = $args['page'] . '.twig';

			$this->view->render($resp, $page);

		}

		/*public function apis($req,$res,$args){
			$api_uri = $
		}*/
	}

?>