<?php

	//functions for basic core auth and verifications
	

	function verify($required,$given){
		$error = false;
		$needAttention = "";
		foreach ($required as $field) {
			if(!isset($given->$field) || strlen(trim($given->field)) <= 0){
				$error = true;
				$needAttention .= $field . ', ';
			}
		}
		if ($error) {
	        // this happens if something is missing in the $given
	        $resp = array();
	        $app = \Slim\Slim::getInstance();
	        $response["status"] = "error";
	        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
	        echoResponse(200, $response);
	        $app->stop();
    	}
	}

	function echoResponse($status_code, $response) {
	    $app = \Slim\Slim::getInstance();
	    // Http response code
	    $app->status($status_code);

	    // setting response content type to json
	    $app->contentType('application/json');

	    echo json_encode($response);
	}

	function generatePass(){
		$digits = 4;
		$rand = str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);

		return $rand;
	}

?>