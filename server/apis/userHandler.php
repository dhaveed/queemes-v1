<?php

$app->get('/userSession', function() {
    $db = new DbHandler();
    $session = $db->getSession();
    $response["_id"] = $session['_id'];
    $response["email"] = $session['email'];
    $response["fullName"] = $session['fullName'];
    $response["matric_staffid"] = $session['matric_staffid'];
    $response["address1"] = $session['address1'];
    $response["address2"] = $session['address2'];
    $response["hall"] = $session['hall'];
    echoResponse(200, $session);
});


$app->post('/signUp', function() use ($app) {
    header("Content-Type: application/json", true);
    $response = array();
    $r = json_decode($app->request->getBody());
    verifyRequiredParams(array('firstName', 'password', 'phone', 'email',  'lastName', 'matricNumber'),$r);
    require_once 'passwordHash.php';
    $db = new DbHandler();
    $password = $r->password;
    $email = $r->email;
    $phone = $r->phone;
    $lastName = $r->lastName;
    $firstName = $r->firstName;
    $matric = $r->matricNumber;
    $r->matric_staffid = $matric;
    $r->fullname = $firstName . ' ' . $lastName;
    //$created_by = $r->created_by;

    $isUserExists = $db->getOneRecord("select 1 from customers where email='$email' and matric_staffid='$matric'");
    if(!$isUserExists){
            $r->password = passwordHash::hash($password);
            $table_name = "customers";
            $column_names = array('fullname', 'email', 'password', 'matric_staffid');
            $result = $db->insertIntoTable($r, $column_names, $table_name);
            if ($result != NULL) {
            $response["status"] = "success";
            $response["message"] = "Successfully Registered";
            echoResponse(200, $response);
        } else {
            $response["status"] = "error";
            $response["message"] = "Failed to create account. Please try again";
            echoResponse(201, $response);
        }
    }else{
        $response["status"] = "error";
        $response["message"] = "User already exists!";
        echoResponse(201, $response);
    }
});

$app->post('/login', function() use ($app) {
require_once 'passwordHash.php';
$r = json_decode($app->request->getBody());
verifyRequiredParams(array('email', 'password'),$r);
$response = array();
$db = new DbHandler();
$password = $r->password;
$email = $r->email;
$user = $db->getOneRecord("select _id,fullName,email,password,matric_staffid,address1,address2, hall from customers where email='$email'");
if ($user != NULL) {
    if(passwordHash::check_password($user['password'],$password)){
        $response['status'] = "success";
        $response['message'] = 'Login was successful';
        $response['_id'] = $user['_id'];
        $response['email'] = $user['email'];
        $response['fullName'] = $user['fullName'];
        $response['matric_staffid'] = $user['matric_staffid'];
        $response['address1'] = $user['address1'];
        $response['address2'] = $user['address2'];
        $response['hall'] = $user['hall'];
        if (!isset($_SESSION)) {
            session_start();
        }
        $_SESSION['_id'] = $user['_id'];
        $_SESSION['email'] = $user['email'];
        $_SESSION['fullName'] = $user['fullName'];
        $_SESSION['matric_staffid'] = $user['matric_staffid'];
        $_SESSION['address1'] = $user['address1'];
        $_SESSION['address2'] = $user['address2'];
        $_SESSION['hall'] = $user['hall'];

        echoResponse(200, $response);
        }
     else {
        $response['status'] = "error";
        $response['message'] = 'Login failed. Incorrect credentials';
        echoResponse(201, $response);
    }
}else {
        $response['status'] = "error";
        $response['message'] = 'User does not exist';
        echoResponse(201, $response);
    }

});
$app->get('/logout', function() {
    $db = new DbHandler();
    $session = $db->logout();
    $response["status"] = "info";
    $response["message"] = "Logged out successfully";
    echoResponse(200, $response);
});
?>