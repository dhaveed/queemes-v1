<?php
$app->get('/getMeal/:rest', function($restaurant) use ($app){
    $db = new DbHandler();
    $response = array();
    $reqRestaurant = $restaurant;
    $resp = $db->getAllRecords("SELECT * from products WHERE parentRestaurant = '$reqRestaurant'");
    if($resp !== null){
        $response["status"] = "success";
        $response["message"] = array();
        while($meals = $resp->fetch_assoc()) {
            $tmp = array();
            $tmp["title"] = $meals["title"];
            $tmp["slug"] = $meals["slug"];
            $tmp["inStock"] = $meals["inStock"];
            $tmp["image"] = $meals["image"];
            
            array_push($response["message"], $tmp);
        }
        echoResponse(200, $response);
    }else {
        $response['status'] = "error";
        $response['message'] = "";
    }
});

$app->post('/slug', function() use ($app) {
$r = json_decode($app->request->getBody());
verifyRequiredParams(array('meal'),$r);
$mealName = $r->meal;
$response = array();
$db = new DbHandler();

$meal = $db->getOneRecord("select * from products where title='$mealName'");
if ($meal != NULL) {
    $response['status'] = "success";
    // $response['message'] = 'Login was successful';
    $response['title'] = $meal['title'];
    $response['description'] = $meal['slug'];
    $response['image'] = $meal['image'];
    $response['inStock'] = $meal['inStock'];
    $response['parentRestaurant'] = $meal['parentRestaurant'];
    echoResponse(200, $response);
}else {
    $response['status'] = "error";
    $response['message'] = 'Selected meal does not exist';
    echoResponse(201, $response);
}

});
?>
