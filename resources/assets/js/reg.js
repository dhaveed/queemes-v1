//core reg. form handling methods
//
$(document).ready(function(){

    //hiding the error boxes
    
    $('.padded').hide();


    $('#confirm-password').on('keyup', function(){
        var p1 = $('#reg-password').val();
        var p2 = $('#confirm-password').val();

        if ((p1.length || p2.length) <= 0) {//#CD4439
            $('#errormsg').hide();
            $('#errormsg').addClass('noPadding');
        }else if((p1 !== p2)){
            $('#errormsg').show();
            $('#errormsg').removeClass('noPadding');
            $('#errormsg').text("Passwords are not the same");
            $('#errormsg').addClass('padded');
        } else {
            $('#errormsg').hide();
        }
    });
});

$("#reg-form").submit(function(e) {
    e.preventDefault();
});

$("#login-form").submit(function(e) {
    e.preventDefault();
});

$('#btn-signup').on('click', function(){
    //using my custom toAjax function in reg.js
    toAjax('#reg-form', 'http://localhost/queemes-v1/server/apis/signUp', 'post');
});

//login form start ------------------------

$('#btn-login').on('click', function(){
    toAjax('#login-form', 'http://localhost/queemes-v1/server/apis/login', 'post');
})
    

//-----------------------------end form handling functions


//to ajax function definition
function toAjax(formId, sendTo, method){
	$(function() {
            var extractFieldNames = function(fieldName, expression, keepFirstElement) 
            {
                expression = expression || /([^\]\[]+)/g;
                keepFirstElement = keepFirstElement || false;
                
                var elements = [];
                while((searchResult = expression.exec(fieldName)))
                {
                    elements.push(searchResult[0]);
                }
                
                if (!keepFirstElement && elements.length > 0) elements.shift();
                
                return elements;
            }
          
            var attachProperties = function(target, properties, value) 
            {
                var currentTarget = target;
                var propertiesNum = properties.length;
                var lastIndex = propertiesNum - 1;
            
                for (var i = 0; i < propertiesNum; ++i) 
                {
                    currentProperty = properties[i];
                    
                    if (currentTarget[currentProperty] === undefined) 
                    {
                        currentTarget[currentProperty] = (i === lastIndex) ? value : {};
                    }
                    
                    currentTarget = currentTarget[currentProperty];                   
                }
            }
            
            var convertFormDataToObject = function(form) {
            
                var currentField = null;
                var currentProperties = null;
                
                // result of this function
                var data = {};
            
                // get array of fields that exist in this form
                var fields = form.serializeArray();
                //console.log(fields);
                for (var i = 0; i < fields.length; ++i) 
                {                
                  currentField = fields[i];                  
                  // extract field names
                  currentProperties = extractFieldNames(currentField.name);                 
                  // add new fields to our data object
                  attachProperties(data, currentProperties, currentField.value);
                }
                
                return data;
            }    
        
            // main conversion
            var form = $(formId);            
            var data = convertFormDataToObject(form);

            var postData = JSON.stringify(data);
            
            // send data with ajax...
            $.ajax({
              url: sendTo,
              method: method,
              dataType:'json',
              data: postData,
              success: function(response){
                  console.log(JSON.stringify(response)); // just like in angular
                  if(response.status == "success"){
                    window.location.assign('foodbasket');
                  } else{
                    var thisFormErr = formId + "-error";
                    console.log(thisFormErr);
                    $(thisFormErr).show();
                    $(thisFormErr).text(response.message);
                    console.log(response.message);
                  }
              },
              error: function(x,s,e){
                console.log(JSON.stringify(x) + '\n' + s + '\n' + e);
              }
            });
        });
};