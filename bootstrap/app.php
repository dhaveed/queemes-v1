<?php

require __DIR__ . './../vendor/autoload.php';
require './../app/controllers/BaseController.php';
require './../app/controllers/RoutesController.php';
// require './../app/controllers/UserController.php';

$app = new \Slim\App([
	'settings' => [
		'displayErrorDetails' => true
	]
]);

$container = $app->getContainer();

//register twig view helper
$container['view'] = function($c){

	$views = new \Slim\Views\Twig(__DIR__ . '/../resources/views/', [
			'cache' => false,
		]
	);

	$basePath = rtrim(str_ireplace('index.php', '', $c['request']->getUri()->getBasePath()), '/');

	/*$views->addExtension(new \Slim\Views\TwigExtension(
			$c->router,
			$c->request->getUri()
		)
	);*/
	    $views->addExtension(new Slim\Views\TwigExtension($c['router'], $basePath));

	return $views;
};
$container['BaseController'] = function($container){
	return new \App\Controllers\BaseController($container->view);
};
$container['RoutesController'] = function($container){
	return new \App\Controllers\RoutesController($container->view);
}

?>