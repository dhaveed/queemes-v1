<?php
session_start();
require '../vendor/autoload.php'; //default composer generated autoloader for slim
$app = new \Slim\App();


require '../app/coreMethods.php';
require '../app/controllers/dbMethods.php';
require '../bootstrap/app.php';
require '../app/routes.php';

$app->run();